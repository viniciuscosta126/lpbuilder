import os
import json


def read_files(arquivos, json_archive):
    arquivos_lidos = []
    for arquivo in arquivos:
        with open(f'{os.getcwd()}//{arquivo}', 'r', encoding='utf-8') as file:
            text = file.read()
            arquivos_lidos.append(text)
    diretorio = os.getcwd()
    return json_archive.append({
        "nome": diretorio.split('/')[-2],
        "tipo": diretorio.split('/')[-1],
        "html": arquivos_lidos[2],
        "css": arquivos_lidos[0],
        "js": arquivos_lidos[1],
    })


def browse_folders():
    json_archive = []
    os.chdir(f'components')

    pastas = os.listdir()
    for pasta in pastas:

        os.chdir(f'{os.getcwd()}//{pasta}')
        subpastas = os.listdir()
        subpastas = sorted(subpastas)

        for subpasta in subpastas:
            os.chdir(f'{os.getcwd()}//{subpasta}')
            arquivos = os.listdir()
            read_files(arquivos, json_archive)
            os.chdir('..//')
        os.chdir('..//')
    components = {
        "components": [

        ]
    }
    for i in json_archive:
        components['components'].append(i)
    os.chdir('..//')

    try:
        with open("data.json", 'r'):
            os.remove('data.json')
            with open("data.json", 'w') as outfile:
                json.dump(components, outfile)
    except:
        with open("data.json", 'w') as outfile:
            json.dump(components, outfile)


def write_html(links_css, links_js, nome_pasta):
    head = """<!DOCTYPE html>\n<html lang="pt">\n<head>\n<meta charset="UTF-8">\n<title>Document</title>\n</head>\n<body>"""

    final = """</body>\n</html>"""

    os.mkdir(nome_pasta)
    os.mkdir(f'{nome_pasta}/CSS')
    os.mkdir(f'{nome_pasta}/JS')

    contador = 0

    with open('data.json', 'r') as file_components:
        dados_components = json.load(file_components)

    with open(f'{nome_pasta}/index.html', 'w') as index:
        index.writelines(head)
        while True:
            print("Oque deseja adicionar??")
            contador = 0
            for i in dados_components['components']:
                print(
                    f'{contador+1} - Componente: {i["nome"]} | Tipo: {i["tipo"]}')
                contador = contador + 1

            opcao = int(input("Digite a opcao desejada -->"))

            index.writelines(dados_components["components"][opcao-1]["html"])

            if dados_components["components"][opcao-1]["css"] != "":
                links_css.append(
                    f'<link rel="stylesheet" href="./CSS/{dados_components["components"][opcao-1]["nome"]}-{dados_components["components"][opcao-1]["tipo"]}.css">')
                with open(f'{nome_pasta}/CSS/{dados_components["components"][opcao-1]["nome"]}-{dados_components["components"][opcao-1]["tipo"]}.css', 'w') as css:
                    css.writelines(
                        dados_components["components"][opcao-1]["css"])

            if dados_components["components"][opcao-1]["js"] != "":
                links_js.append(
                    f'<script type="text/javascript" src="./JS/{dados_components["components"][opcao-1]["nome"]}-{dados_components["components"][opcao-1]["tipo"]}.js"></script>')
                with open(f'{nome_pasta}/JS/{dados_components["components"][opcao-1]["nome"]}-{dados_components["components"][opcao-1]["tipo"]}.js', 'w') as js:
                    js.writelines(
                        dados_components["components"][opcao-1]["js"])

            opcao_sair = input("Deseja finalizar --> S/N???")

            if opcao_sair.upper() == "S":
                index.writelines(final)
                break

    return links_css


def import_css(links_css, nome_pasta):
    list = []
    with open(f'{nome_pasta}/index.html', 'r') as index:
        list = index.readlines()
        for i in links_css:
            list.insert(4, i)
    with open(f'{nome_pasta}/index.html', 'w') as index:
        index.writelines(list)


def import_js(links_js, nome_pasta):
    list = []
    with open(f'{nome_pasta}/index.html', 'r') as index:
        list = index.readlines()
        for i in links_js:
            list.insert(len(list) - 3, i)
    with open(f'{nome_pasta}/index.html', 'w') as index:
        index.writelines(list)


if __name__ == "__main__":
    links_css = []
    links_js = []
    nome_pasta = input(
        "Digite o nome da pasta que deseja salvar os arquivos -->")
    browse_folders()
    write_html(links_css, links_js, nome_pasta)
    import_css(links_css, nome_pasta)
    import_js(links_js, nome_pasta)
